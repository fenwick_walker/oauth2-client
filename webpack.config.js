const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname + '/src/js'),
  resolve: {
    alias: {
      Root: path.resolve(__dirname + '/src/js'),
      Styles: path.resolve(__dirname + '/src/styles'),
    },
    extensions: ['.js', '.jsx']
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'public/'),
    public: 'localhost',
    port: 8080,
    publicPath: 'http://localhost:8080/js/',
    historyApiFallback: true,
    stats: {
      colors: true,
    },
  },
  entry: [
    path.resolve(__dirname + '/src/js/index.jsx')
  ],
  output: {
    path: path.join( __dirname, 'public/js'),
    publicPath: '/js/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        use: ['babel-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  }
};

import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
  }).isRequired,
};

function EmailItem({ data }) {
  return (
    <div>
      <h3>{data.title}</h3>
    </div>
  );
}

EmailItem.propTypes = propTypes;

export default EmailItem;

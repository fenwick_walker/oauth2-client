import React from 'react';

import Item from './Item';

function EmailList({ items }) {
  return items.map(item => (
    <Item
      key={Math.random().toString(36).substring(4)}
      data={item}
    />
  ));
}

export default EmailList;

import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  onClick: PropTypes.func.isRequired,
};

function Button({ onClick }) {
  return (
    <button
      className="button is-primary is-fullwidth"
      onClick={e => onClick(e)}
    >
      Click-2-auth
    </button>
  );
}

Button.propTypes = propTypes;

export default Button;

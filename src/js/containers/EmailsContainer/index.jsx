import React from 'react';
import { Redirect } from 'react-router-dom';

import Auth from 'Root/auth';
import Emails from 'Root/emails';

import EmailList from 'Root/components/Email/List';

class EmailsContainer extends React.Component {
  constructor() {
    super();

    this.auth = new Auth();
    this.emails = new Emails();

    this.state = {
      authed: false,
      loading: true,
    };
  }

  componentDidMount() {
    this.auth.init().then(() => {
      // move outta here
      this.fetchEmails().then((messages) => {
        console.log(messages);
        this.setState({
          authed: this.auth.isAuthed,
          loading: false,
        });
      });
    });
  }

  fetchEmails() {
    // console.log(this.auth.instance);
    return this.emails.fetch()
  }

  render() {
    const {
      loading,
      authed,
    } = this.state;

    if (!loading && !authed) {
      return <Redirect to="/login" />;
    }

    return loading
      ? <h1>Loading...</h1>
      : <EmailList
        items={[
          { title: 'one' },
          { title: 'two' },
          { title: 'three' },
        ]}
      />;
  }
}

export default EmailsContainer;

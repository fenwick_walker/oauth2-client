import React from 'react';

import Auth from 'Root/auth';

import Title from 'Root/components/Login/Title';
import Button from 'Root/components/Login/Button';

class LoginContainer extends React.Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      loggedIn: false,
    };

    this.auth = new Auth();

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  componentDidMount() {
    this.auth.init().then(() => {
      this.setState({
        loading: false,
        loggedIn: this.auth.isAuthed,
      });
    });
  }

  handleButtonClick() {
    this.auth.signIn().then((isAuthed) => {
      this.setState({
        loggedIn: isAuthed,
      });
    });
  }

  renderText() {
    return this.state.loggedIn
      ? <h2>Totally authed!</h2>
      : null;
  }

  renderButton() {
    if (this.state.loggedIn) return null;

    return this.state.loading
      ? <h2>Loading...</h2>
      : <Button onClick={this.handleButtonClick} />;
  }

  render() {
    // Render auth button or email list
    return (
      <div className="login">
        <div className="login__container">
          <div className="panel">
            <div className="panel-heading">
              <Title text="Totally Authome" />
            </div>
            <div className="panel-block">
              {this.renderText()}
              {this.renderButton()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginContainer;

const { gapi, atob } = window;

class Emails {
  fetch() {
    this.hello = true;

    return new Promise((resolve) => {
      gapi.client.gmail.users.messages.list({
        userId: 'me',
        maxResults: 10,
        q: 'is:unread',
      }).then((response) => {
        const { messages } = response.result;
        const emails = messages.map(message =>
          gapi.client.gmail.users.messages.get({
            userId: 'me',
            id: message.id,
            format: 'full',
          }).then(emailResponse =>
            console.log(atob(emailResponse.result.payload.parts[1].body.data.replace(/-/g, '+').replace(/_/g, '/')))));

        resolve(emails);
      });
    });
  }
}

export default Emails;

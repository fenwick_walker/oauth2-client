import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import EmailsContainer from 'Root/containers/EmailsContainer';
import LoginContainer from 'Root/containers/LoginContainer';

import 'Styles/aoa2.scss';

const App = () => (
  <Router>
    <div>
      <Route exact path="/login" component={LoginContainer} />
      <Route exact path="/emails" component={EmailsContainer} />
    </div>
  </Router>
);

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);

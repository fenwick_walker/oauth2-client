const API_KEY = 'AIzaSyADVL7rOApcAe5TN15yvFJlnoya1Ob1_AY';
const CLIENT_ID = '989288342812-0v2ee47p21ndtc80o0detefiecq9jthm.apps.googleusercontent.com';
const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'];
const SCOPE = 'https://www.googleapis.com/auth/gmail.readonly';

const { gapi } = window;

const initClient = () => new Promise((resolve) => {
  gapi.load('client:auth2', () => {
    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPE,
    }).then(() => {
      resolve();
    });
  });
});

class Auth {
  constructor() {
    this.isAuthed = false;
  }

  init() {
    return initClient().then(() => this.setAuthStatus());
  }

  setAuthStatus() {
    this.isAuthed = gapi.auth2
      .getAuthInstance().isSignedIn.get();
  }

  signIn() {
    return new Promise((resolve) => {
      if (!this.authed) {
        this.clientInstance.signIn().then(() =>
          resolve(this.authed));
      }
    });
  }

  signOut() {
    return new Promise((resolve) => {
      if (this.authed) {
        this.clientInstance.signOut();
      }

      resolve();
    });
  }

  get clientInstance() {
    return gapi.auth2.getAuthInstance();
  }

  get authed() {
    return this.clientInstance.isSignedIn.get();
  }
}

export default Auth;
